const { createProxyMiddleware } = require('http-proxy-middleware');

let target = 'https://asi2-sca.swla.be'; //'http://localhost:9000';

module.exports = function(app){
    app.use(createProxyMiddleware('/api', {target:target, ws:false, changeOrigin: true, secure: false}));
    app.use(createProxyMiddleware('/socket.io', {target:target, ws:true, changeOrigin: true, secure: false}));
    //app.use(createProxyMiddleware('/api', {target:target}));
    //app.use(createProxyMiddleware('/node', {target:'http://localhost:3001'}));
}
