import { io } from "socket.io-client";

const URL = "/";
const socket = io(URL, {
  autoConnect: false,
  extraHeaders: {
    "my-custom-header": "abcd",
  },
});

export default socket;
