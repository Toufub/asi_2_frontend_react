export const selectedClient = (state) => state.selectedClient;
export const selectedClientOpposant = (state) => state.selectedClientOpposant;

export const estDemandeCombatEnvoyee = (state) => state.demandeCombatEnvoyee;
export const estDemandeCombatRecue = (state) => state.demandeCombatRecue;

export const selectedCardId = (state) => state.selectedCardId;


export const selectedCardsJeu = (state) => state.selectedCardsJeu;
export const selectedCardsOpposantJeu = (state) => state.selectedCardsOpponentJeu;
export const cardsValidated = (state) => state.cardsValidated;
export const gameReady = (state) => state.gameReady;
export const pointAction = (state) => state.pointAction;
export const pointActionAdversaire = (state) => state.pointActionAdversaire;
export const turn = (state) => state.turn;
export const cardAttaquante = (state) => state.carteAttaquante;
export const cardAttaquee = (state) => state.carteAttaquee;
export const gameWinned = (state) => state.gameWinned;

//export const allmessages = (state) => state.messages;