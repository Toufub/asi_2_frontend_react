import { setCookie } from "../utils/cookie.js";
import { clientActions } from "./client.actions.js";

const initStateValue = {
  selectedClient: {},
  selectedClientOpposant: null,
  selectedCardId: null,
  demandeCombatEnvoyee: false,
  demandeCombatRecue: false,
  selectedCardsJeu: [],
  selectedCardsOpponentJeu: [],
  cardsValidated: false,
  gameReady: false,
  pointAction: 0,
  pointActionAdversaire: 0,
  turn: false,
  carteAttaquee: null,
  carteAttaquante: null,
  gameWinned: null,
  messages: [],
};
export const clientReducer = (
  state = {
    state: initStateValue,
  },
  action
) => {
  if (action.type === clientActions.setSelectedClient) {
    setCookie("clientName", action.payload?.login);
    return {
      ...state,
      selectedClient: action.payload,
    };
  }

  if (action.type === clientActions.setSelectedClientOpposant) {
    return {
      ...state,
      selectedClientOpposant: action.payload,
    };
  }

  if (action.type === clientActions.setSelectedCardId)
    return {
      ...state,
      selectedCardId: action.payload,
    };

  if (action.type === clientActions.removeSelectedCardId)
    return {
      ...state,
      selectedCardId: null,
    };

  if (action.type === clientActions.setDemandeCombatEnvoyee)
    return {
      ...state,
      demandeCombatEnvoyee: action.payload,
    };

  if (action.type === clientActions.setDemandeCombatRecue)
    return {
      ...state,
      demandeCombatRecue: action.payload,
    };

  if (action.type === clientActions.setSelectedCardsJeu)
    return {
      ...state,
      selectedCardsJeu: action.payload,
    };
  if (action.type === clientActions.setSelectedCardsOpponentJeu)
    return {
      ...state,
      selectedCardsOpponentJeu: action.payload,
    };
  if (action.type === clientActions.setCardsValidated)
    return {
      ...state,
      cardsValidated: action.payload,
    };
  if (action.type === clientActions.setGameReady)
    return {
      ...state,
      gameReady: action.payload,
    };

  if (action.type === clientActions.setPointAction)
    return {
      ...state,
      pointAction: action.payload,
    };
  if (action.type === clientActions.setPointActionAdversaire)
    return {
      ...state,
      pointActionAdversaire: action.payload,
    };

  if (action.type === clientActions.setTurn)
    return {
      ...state,
      turn: action.payload,
    };

    if (action.type === clientActions.setCarteAttaquante)
    return {
      ...state,
      carteAttaquante: action.payload,
    };
    if (action.type === clientActions.setCarteAttaquee)
    return {
      ...state,
      carteAttaquee: action.payload,
    };

    if (action.type === clientActions.setGameWinned)
    return {
      ...state,
      gameWinned: action.payload,
    };
    
    /*
    if (action.type === clientActions.setMessages)
    return {
      ...state,
      messages: action.payload,
    };
    */
  return state;
};
