export const clientActions = {
  setSelectedClient: "@@clients/SET_SELECTED_CLIENT",
  setSelectedClientOpposant: "@@clients/SET_SELECTED_CLIENT_OPPOSANT",
  setSelectedCardId: "@@cards/SET_SELECTED_CARD_ID",
  removeSelectedCardId: "@@cards/REMOVE_SELECTED_CARD_ID",
  setDemandeCombatEnvoyee: "@@clients/SET_DEMANDE_COMBAT_ENVOYEE",
  setDemandeCombatRecue: "@@clients/SET_DEMANDE_COMBAT_RECUE",
  setSelectedCardsJeu: "@@cards/SET_SELECTED_CARDS_JEU",
  setSelectedCardsOpponentJeu: "@@cards/SET_SELECTED_CARDS_OPPONENT_JEU",
  setCardsValidated: "@@cards/SET_CARDS_VALIDATED",
  setGameReady: "@@cards/SET_GAME_READY",
  setPointAction: "@@clients/SET_POINT_ACTION",
  setPointActionAdversaire: "@@clients/SET_POINT_ACTION_ADVERSAIRE",
  setTurn: "@@jeu/SET_TURN",
  setCarteAttaquante: "@@jeu/SET_CARTE_ATTAQUANTE",
  setCarteAttaquee: "@@jeu/SET_CARTE_ATTAQUEE",
  setGameWinned: "@@jeu/SET_GAME_WINNED",
  setMessages: "@@chat/SET_MESSAGES",
};

export const setSelectedClient = (selectedClient) => {
  return {
    type: clientActions.setSelectedClient,
    payload: selectedClient,
  };
};

export const setSelectedClientOpposant = (selectedClientOpposant) => {
  return {
    type: clientActions.setSelectedClientOpposant,
    payload: selectedClientOpposant,
  };
};

export const setSelectedCardId = (selectedCardId) => {
  return {
    type: clientActions.setSelectedCardId,
    payload: selectedCardId,
  };
};

export const removeSelectedCardId = (selectedCardId) => {
  return {
    type: clientActions.removeSelectedCardId,
    payload: selectedCardId,
  };
};

export const setDemandeCombatEnvoyee = (demandeCombatEnvoyee) => {
  return {
    type: clientActions.setDemandeCombatEnvoyee,
    payload: demandeCombatEnvoyee,
  };
};
export const setDemandeCombatRecue = (demandeCombatRecue) => {
  return {
    type: clientActions.setDemandeCombatRecue,
    payload: demandeCombatRecue,
  };
};
export const setSelectedCardsJeu = (selectedCardsJeu) => {
  return {
    type: clientActions.setSelectedCardsJeu,
    payload: selectedCardsJeu,
  };
};
export const setSelectedCardsOpponentJeu = (selectedCardsOpponentJeu) => {
  return {
    type: clientActions.setSelectedCardsOpponentJeu,
    payload: selectedCardsOpponentJeu,
  };
};
export const setCardsValidated = (cardsValidated) => {
  return {
    type: clientActions.setCardsValidated,
    payload: cardsValidated,
  };
};
export const setGameReady = (gameReady) => {
  return {
    type: clientActions.setGameReady,
    payload: gameReady,
  };
};
export const setPointAction = (pointAction) => {
  return {
    type: clientActions.setPointAction,
    payload: pointAction,
  };
};
export const setPointActionAdversaire = (pointAction) => {
  return {
    type: clientActions.setPointActionAdversaire,
    payload: pointAction,
  };
};
export const setTurn = (turn) => {
  return {
    type: clientActions.setTurn,
    payload: turn,
  };
};
export const setCarteAttaquante = (card) => {
  return {
    type: clientActions.setCarteAttaquante,
    payload: card,
  };
};
export const setCarteAttaquee = (card) => {
  return {
    type: clientActions.setCarteAttaquee,
    payload: card,
  };
};

export const setGameWinned = (userId) => {
  return {
    type: clientActions.setGameWinned,
    payload: userId,
  };
};

/*
export const setMessages = (messages) => {
  return {
    type: clientActions.setMessage,
    payload: messages,
  };
};
*/