
function CombatFin(props) {


  return (
    <div style={{ position: "absolute", top: "50%", right: "38%" }}>
      <div style={{ backgroundColor: "rgb(219, 218, 218)", border: "1px solid black" , position: "relative" }}>
        <div className="d-flex align-items-center justify-content-between h-100 w-100 m-5" style={{fontSize:"200%"}}><b>{props.message}</b></div>
      </div>
    </div>
  );
}

export default CombatFin;
