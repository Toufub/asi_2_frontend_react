import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setCarteAttaquante, setCarteAttaquee } from "../../../core/client.actions";
import { cardAttaquante, cardAttaquee, selectedCardsJeu, selectedCardsOpposantJeu, turn } from "../../../core/client.selector";
import socket from "../../../socket";
import CardGeneral from "../../card/card.general";

function CardListJeuDemarre(props) {
  const cardsSelectionnees = useSelector(selectedCardsJeu);
  const cardsSelectionneesOpposant = useSelector(selectedCardsOpposantJeu);
  const yourTurn = useSelector(turn);

  const carteAttaquee = useSelector(cardAttaquee);
  const carteAttaquante = useSelector(cardAttaquante);
  const [carteAttaqueeErreur, setCarteAttaqueeErreur] = useState(false);
  const [carteAttaquanteErreur, setCarteAttaquanteErreur] = useState(false);


  const dispatch = useDispatch();


  const send_data = (type, action, message) => {
    console.log("send-data", action);
    socket.emit("data-card-app", {
      type: type,
      action: action,
      message: message,
    });
  };

  const attaquer = () => {
    setCarteAttaquanteErreur(false)
    setCarteAttaqueeErreur(false)

    if (carteAttaquee === null || carteAttaquante === null) return;
    if(carteAttaquee.hp <= 0) {
      setCarteAttaqueeErreur(true)
      return;
    }
    if(carteAttaquante.hp <= 0) {
      setCarteAttaquanteErreur(true)
      return;
    }

    send_data("game", "attack", { card_src: carteAttaquante, card_dest: carteAttaquee });
    
  };

  const selectionnerCardAttaquante = (card) => {
    if (!yourTurn) return;
    dispatch(setCarteAttaquante(card));
  };

  const selectionnerCardAttaquee = (card) => {
    if (!yourTurn) return;
    dispatch(setCarteAttaquee(card));
  };

  const listItems = cardsSelectionnees?.map((card) => (
    <div key={card.id} style={{ height: "300px" }} className="me-2" onClick={() => selectionnerCardAttaquante(card)}>
      <CardGeneral card={card} selectionne={carteAttaquante?.id === card.id} />
    </div>
  ));
  const listItemsOpposant = cardsSelectionneesOpposant?.map((card) => (
    <div key={card.id} style={{ height: "300px" }} className="me-2" onClick={() => selectionnerCardAttaquee(card)}>
      <CardGeneral card={card} selectionne={carteAttaquee?.id === card.id}/>
    </div>
  ));
  return (
    <div className="d-flex align-items-center w-75">
      {!props.opposant ? listItems : listItemsOpposant}
      { yourTurn && !props.opposant && (<div className="w-100">
        <button className="btn btn-success" onClick={attaquer}>Confirmer l'attaque</button>
        {carteAttaquanteErreur && (<div className="text-danger">Votre carte ne peut pas être utilisée, elle n'a plus de point de vie.</div>)}
        {carteAttaqueeErreur && (<div className="text-danger">La carte de l'avdersaire n'a déjà plus de point de vie.</div>)}
      </div>)}
    </div>
  );
}

export default CardListJeuDemarre;
