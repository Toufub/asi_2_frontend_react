import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { url } from "../../../conf";
import { setSelectedCardsJeu } from "../../../core/client.actions";
import { cardsValidated, selectedCardsJeu, selectedClient } from "../../../core/client.selector";
import CardGeneral from "../../card/card.general";

function CardListJeu(props) {
  const [cards, setCards] = useState();
  const user = useSelector(selectedClient);
  const cardsValidees = useSelector(cardsValidated);

  const cardsSelectionnees = useSelector(selectedCardsJeu);

  const dispatch = useDispatch();

  const selectCards = (card) => {
    if (!props.selectionCard) return;
    // console.log("New card selected", card.id, cardsSelectionnees);

    let index = cardsSelectionnees?.findIndex((x) => x.id === card.id);
    if (cardsSelectionnees?.length > 0 && index !== -1) {
      if(cardsSelectionnees?.length === 1) {
        dispatch(setSelectedCardsJeu([]));
        return
      }

      let tmp = [...cardsSelectionnees];
      tmp.splice(index,index+1)
      dispatch(setSelectedCardsJeu([...tmp]));

    } else {
      if (cardsSelectionnees == null) dispatch(setSelectedCardsJeu([card]));
      else dispatch(setSelectedCardsJeu([...cardsSelectionnees, card]));
    }
  };

  useEffect(() => {
    fetch(`${url}/cards`)
      .then((resp) => resp.json())
      .then((values) => {
        setCards([]);
        setCards(values.filter((x) => x.userId === user?.id));
      });
  }, [user]);

  const listItemsAvantCardsValidees = cards?.map((card) => (
    <div key={card.id} onClick={() => selectCards(card)} style={{ height: "300px" }} className="me-2">
      <CardGeneral card={card} selectionne={cardsSelectionnees?.find(x => x.id === card.id)}/>
    </div>
  ));
  const listItemsCardsValidees = cards?.map((card) => (
    <div key={card.id} style={{ height: "300px" }} className="me-2">
      <CardGeneral card={card} />
    </div>
  ));
  return <div className="d-flex align-items-center w-25">{cardsValidees && !props.selectionCard ? listItemsCardsValidees : listItemsAvantCardsValidees}</div>;
}

export default CardListJeu;
