import { useSelector } from "react-redux";
import { selectedClientOpposant } from "../../../core/client.selector";

function DemandeReponseCombat(props) {

  const opponent = useSelector(selectedClientOpposant);

  
  
  return (
    <div className="d-flex align-items-center justify-content-center">
      <div className="d-flex align-items-center justify-content-center flex-column me-1">
        <b>
          {opponent?.surName} {opponent?.lastName}
        </b>
      </div>
      <div>vous demande en combat !</div>
      <button className="btn btn-success ms-1" onClick={props.accepterCombat}>
        Accept
      </button>
      <button className="btn btn-danger ms-1" onClick={props.refuserCombat}>
        Deny
      </button>
    </div>
  );
}

export default DemandeReponseCombat;
