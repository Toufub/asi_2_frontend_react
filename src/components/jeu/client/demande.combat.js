import { useSelector } from "react-redux";
import { selectedClientOpposant } from "../../../core/client.selector";

function DemandeCombat(props) {

  const opponent = useSelector(selectedClientOpposant);

  
  return (
    <div className="d-flex align-items-center justify-content-center">
      <div>Vous avez demandé </div> 

      <div className="d-flex align-items-center justify-content-center flex-column me-1 ms-1">
        <b>
          {opponent?.surName} {opponent?.lastName}
        </b>
      </div>
      <div> en combat !</div>
      <button className="btn btn-danger ms-1" onClick={props.annulerCombat}>
        Cancel
      </button>
    </div>
  );
}

export default DemandeCombat;
