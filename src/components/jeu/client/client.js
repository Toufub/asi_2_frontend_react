import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ProgressBar } from "react-bootstrap";
import { useSelector } from "react-redux";
import { cardsValidated, gameReady, pointAction, pointActionAdversaire, selectedCardsJeu, turn } from "../../../core/client.selector";
import socket from "../../../socket";
import CardListJeu from "../card/card.list";
import CardListJeuDemarre from "../card/card.list.demarre";

function Client(props) {

  const cardsSelectionnees = useSelector(selectedCardsJeu);
  const cardsValidees = useSelector(cardsValidated);
  const gamePrete = useSelector(gameReady);
  const pointDAction = useSelector(pointAction);
  const pointDActionAdversaire = useSelector(pointActionAdversaire);
  const yourTurn = useSelector(turn);

  const send_data = (type, action, message) => {
    console.log("send-data", action);
    socket.emit("data-card-app", {
      type: type,
      action: action,
      message: message,
    });
  };

  const validerSelectionCard = () => {
    if (cardsSelectionnees?.length === 0) return;
    send_data("game", "cards", cardsSelectionnees);
  };

  return (
    <div className="d-flex align-items-center justify-content-between w-100">
      <div className="d-flex align-items-center justify-content-center flex-column" style={{ flexBasis: "20%" }}>
        <FontAwesomeIcon icon="user" style={{ fontSize: "200%" }} />
        {props.user?.surName} {props.user?.lastName}
        {gamePrete && !props.opposant && (<div className="m-2 w-100 p-2"> <ProgressBar now={100} label={`${pointDAction ?? 0} points d'action`}/></div>)}
      </div>
      <div className="w-100">
      {props.selectionCard && !cardsValidees && !gamePrete && !props.opposant && <div className="d-flex align-items-center justify-content-center w-100">Select maximum 5 cards</div>}
        {gamePrete && !props.opposant && <div className="d-flex align-items-center justify-content-center w-100">{yourTurn ? "A vous de jouer, Veuillez choisir votre carte qui va attaquer une carte de votre adversaire." : "L'adversaire est en train de jouer"}</div>}

        {!props.opposant && !gamePrete && (<CardListJeu selectionCard={props.selectionCard}/>)}

        {!props.opposant && gamePrete && (<CardListJeuDemarre opposant={props.opposant}/>)}
        {props.opposant && gamePrete &&(<CardListJeuDemarre opposant={props.opposant}/>)}

      </div>
      {props.selectionCard && !cardsValidees && !gamePrete && !props.opposant && (
        <div className="d-flex align-items-center justify-content-center flex-column" style={{ flexBasis: "30%" }}>
          <div>{cardsSelectionnees?.length} cards selected</div>
          <button className="btn btn-success" onClick={() => validerSelectionCard()}>
            Confirm
          </button>
        </div>
      )}
    </div>
  );
}

export default Client;
