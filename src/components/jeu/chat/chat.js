import { padding } from "@mui/system";
import React, { useEffect, useState } from "react";
import { /*useDispatch, */useSelector } from "react-redux";
import { selectedClient, selectedClientOpposant, /*allmessages*/ } from "../../../core/client.selector";
import socket from "../../../socket";
//import { setMessages } from "../../../core/client.actions";

function Chat() {
  //const dispatch = useDispatch();

  //Récupération des variables du store
  const user = useSelector(selectedClient);
  const opponent = useSelector(selectedClientOpposant);
  //const messages = useSelector([...allmessages]);

  //const [tmpMessages, setTmpMessage] = useState([...messages]);
  const [tmpMessages, setTmpMessage] = useState([]);

  let input = document.getElementById('input');

  //Envoie des données grâce au socket
  const send_data = (type, action, message) => {
    console.log("send-data", action);
    socket.emit("data-card-app", {
      type: type,
      action: action,
      message: message,
    });
  };

  useEffect(() => {
    send_data("msg", "tchatting-with", opponent?.id);
  }, [opponent]);

  ///////////////////////////////////
  // Action possibles lors du chat //
  ///////////////////////////////////
  const sendMessage = () => {
    const message = input.value;
    //setTmpMessage([...messages, message]);
    //setTmpMessage( arr => [...arr, message]);
    tmpMessages.push(message)
    setTmpMessage([...new Set(tmpMessages)])
    //dispatch(setMessages(tmpMessages));
    send_data("msg", "message", message);
    input.value = "";
  };

  /////////////////////////////////
  // Socket.on -> réception de   //
  // tous les messages           //
  /////////////////////////////////

  useEffect(() => {
    socket.on("message", function (message) {
      console.log("message", message);
      //setTmpMessage([...messages, message]);
      //setTmpMessage( arr => [...arr, message]);
      tmpMessages.push(message)
      setTmpMessage([...new Set(tmpMessages)])
      //dispatch(setMessages(tmpMessages));
    });
  });

  //Appelé au unmount de l'application
  useEffect(() => {
    return function clean() {
      send_data("user", "disconnect", user?.id);
      window.location.reload(false);
    };
  }, [user]);

  //const listMessages = messages?.map((message) => (
  const listMessages = tmpMessages?.map((message) => (
    <div key={message} className="me-2">
      {message}
    </div>
  ));

  return (
    <div>
      {listMessages}
      <div className="d-flex">
        <div className="col-8">
          <input type="text" className="form-control" id="input" placeholder="Your text message ..." />
        </div>
        <button className="btn btn-info ms-1" onClick={sendMessage}>
          Send
        </button>
      </div>
    </div>
  );
}

export default Chat;
