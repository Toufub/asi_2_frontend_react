import Client from "./client/client";

function InterfaceJoueur(props) {

  return (
    <div>
      <Client user={props.user} selectionCard={props.selectionCard} opposant={props.opposant}/>
    </div>
  );
}

export default InterfaceJoueur;
