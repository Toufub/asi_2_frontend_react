import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Outlet, useLocation } from "react-router-dom";
import { url } from "../../conf";
import { setSelectedClient } from "../../core/client.actions";
import { selectedClient } from "../../core/client.selector";
import { getCookie } from "../../utils/cookie";

function Accueil() {
  const sampleLocation = useLocation();

  const clientCookie = getCookie("clientName");
  const user = useSelector(selectedClient);

  const dispatch = useDispatch();

  useEffect(() => {
    if (clientCookie) {
      fetch(`${url}/user/${clientCookie}`)
        .then((resp) => resp.json())
        .then((values) => {
          dispatch(setSelectedClient(values));
        });
    }
  }, [clientCookie, dispatch]);

  return (
    <div className="h-100">
      <div className="d-flex justify-content-between align-items-center p-3" style={{ height: "100px", backgroundColor: "rgb(219 218 218)" }}>
        <h2>
          <Link to="/">
            <FontAwesomeIcon style={{color: "rgb(0 0 0)" }} icon="home" />
          </Link>
        </h2>
        {sampleLocation.pathname.split("/")[1] === "" && <div style={{ fontSize: "300%" }}>Home</div>}
        {sampleLocation.pathname.split("/")[1] === "buy" && <div style={{ fontSize: "300%" }}>Buy</div>}
        {sampleLocation.pathname.split("/")[1] === "sell" && <div style={{ fontSize: "300%" }}>Sell</div>}
        <div className="d-flex align-items-center">
          <Link to={"/connexion"}>
            <FontAwesomeIcon icon="user" className="me-3" style={{ fontSize: "200%",  color: "rgb(0 0 0)"}} />
          </Link>
          {user && (
            <div>
              {user.surName ? (
                <h2 className="cursor-pointer">
                  <Link to="/connexion">{user.surName}</Link>
                </h2>
              ) : (
                <h2>
                  {" "}
                  <Link to="/connexion">Login</Link>
                </h2>
              )}
              <div className="d-flex align-items-center">
                {user.balance}
                <FontAwesomeIcon style={{color: "rgb(0 0 0)" }} icon="euro-sign" className="ms-1" />
              </div>
            </div>
          )}
        </div>
      </div>
      <Outlet />
    </div>
  );
}

export default Accueil;
