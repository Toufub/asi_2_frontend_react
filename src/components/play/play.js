import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { estDemandeCombatEnvoyee, estDemandeCombatRecue, gameWinned, selectedCardsJeu, selectedCardsOpposantJeu, selectedClient, selectedClientOpposant } from "../../core/client.selector";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import InterfaceJoueur from "../jeu/interfaceJoueur";
import { url } from "../../conf";
import DemandeCombat from "../jeu/client/demande.combat";
import DemandeReponseCombat from "../jeu/client/demande.reponse.combat";
import {
  setCardsValidated,
  setDemandeCombatEnvoyee,
  setDemandeCombatRecue,
  setGameReady,
  setGameWinned,
  setPointAction,
  setSelectedCardsJeu,
  setSelectedCardsOpponentJeu,
  setSelectedClientOpposant,
  setTurn,
} from "../../core/client.actions";
import socket from "../../socket";
import { v4 as uuidv4 } from "uuid";
import { Snackbar } from "@mui/material";
import { Alert } from "react-bootstrap";
import CombatFin from "../jeu/combat/combat.fin";
import Chat from "../jeu/chat/chat";

function Play() {
  const dispatch = useDispatch();

  //Récupération des variables du store
  const user = useSelector(selectedClient);
  const demandeCombatEnvoyee = useSelector(estDemandeCombatEnvoyee);
  const demandeCombatRecue = useSelector(estDemandeCombatRecue);
  const opponent = useSelector(selectedClientOpposant);
  const cardsSelectionnees = useSelector(selectedCardsJeu);
  const cardsSelectionneesOpposant = useSelector(selectedCardsOpposantJeu);
  const gameGagne = useSelector(gameWinned);

  //Création des variables
  const [usersIdConnected, setUsersIdConnected] = useState([]);
  const [usersConnected, setUsersConnected] = useState([]);
  const [selectionCard, setSelectionCard] = useState(false);
  const [open, setOpen] = useState(false);
  const [severity, setSeverity] = useState("success");
  const [message, setMessage] = useState("");

  //Méthode pour afficher ou non la snackBar
  const handleClose = () => {
    setOpen(false);
  };

  //Envoie des données grâce au socket
  const send_data = (type, action, message) => {
    console.log("send-data", action);
    socket.emit("data-card-app", {
      type: type,
      action: action,
      message: message,
    });
  };

  /////////////////////////////////
  // Action possibles lors de la //
  // demande de combat           //
  /////////////////////////////////
  const demanderCombat = (opponentId) => {
    if (demandeCombatRecue || demandeCombatEnvoyee) return;
    send_data("game", "create-game", opponentId);
  };
  const annulerCombat = () => {
    send_data("game", "cancel-game", opponent?.id);
  };
  const accepterCombat = () => {
    send_data("game", "accept-game", opponent?.id);
  };
  const refuserCombat = () => {
    send_data("game", "deny-game", opponent?.id);
  };

  /////////////////////////////////
  // Socket.on -> réception de   //
  // tous les messages           //
  /////////////////////////////////

  useEffect(() => {
    socket.on("disconnect-event", function (login) {
      console.log("disconnect-event", login);
      let myIndex = usersIdConnected.indexOf(login);
      let tmp = [...usersIdConnected];
      tmp.splice(myIndex, 1);

      setUsersIdConnected([...tmp]);
    });

    socket.on("welcome-message", function (usersConnected) {
      console.log("welcome-message", usersConnected);

      let myIndex = usersConnected.indexOf(user?.id);
      if (myIndex !== -1) {
        usersConnected.splice(myIndex, 1);
      }
      setUsersIdConnected(usersConnected);
    });

    socket.on("connect-event", function (login) {
      console.log("connect-event", login);
      if (login === user?.id) return;

      let index = usersIdConnected.indexOf(login);
      if (index === -1) {
        setUsersIdConnected([...usersIdConnected, login]);
      }
    });

    socket.on("game-ask", function (userIdAsking) {
      console.log("game-ask", userIdAsking);
      dispatch(setDemandeCombatRecue(true));
      dispatchOpposant(userIdAsking);
    });

    socket.on("create-game-confirm", function (result) {
      let ok = result.ok;
      let opponent = result.opponent;
      console.log("create-game-confirm", ok, opponent);
      if (ok === "ok") {
        dispatch(setDemandeCombatEnvoyee(true));
        dispatchOpposant(opponent);
      } else {
        dispatch(setSelectedClientOpposant(null));
        dispatch(setDemandeCombatEnvoyee(false));
      }
    });

    socket.on("game-cancel", function (userIdAsking) {
      console.log("game-cancel", userIdAsking);
      dispatch(setDemandeCombatRecue(false));
      setMessage("La demande de combat a été annulée.");
      setSeverity("danger");
      setOpen(true);
      dispatch(setSelectedClientOpposant(null));
    });

    socket.on("cancel-game-confirm", function (message) {
      console.log("cancel-game-confirm", message);
      dispatch(setSelectedClientOpposant(null));
      dispatch(setDemandeCombatEnvoyee(false));
    });

    socket.on("accept-game-confirm", function (message) {
      console.log("accept-game-confirm", message);
      if (message === "ok") return;
      else {
        dispatch(setSelectedClientOpposant(null));
        dispatch(setDemandeCombatRecue(false));
      }
    });

    socket.on("game-confirm", function (opponentId) {
      console.log("game-confirm", opponentId);
      //dispatch(setDemandeCombatEnvoyee(false));

      setMessage("Votre demande de combat a été acceptée.");
      setSeverity("danger");
      setOpen(true);
    });

    socket.on("game-deny", function (message) {
      console.log("game-deny", message);
      dispatch(setDemandeCombatEnvoyee(false));
      setMessage("Votre demande de combat a été refusée.");
      setSeverity("danger");
      setOpen(true);
      dispatch(setSelectedClientOpposant(null));
    });

    socket.on("deny-game-confirm", function (message) {
      console.log("deny-game-confirm", message);
      if (message === "ok") {
        dispatch(setSelectedClientOpposant(null));
        dispatch(setDemandeCombatRecue(false));
      }
    });

    socket.on("game_initialize", function (opponentId) {
      console.log("game_initialize", opponentId);
      setSelectionCard(true);
    });
    socket.on("card_receive", function (message) {
      console.log("card_receive", message);
      if (message === "ok") dispatch(setCardsValidated(true));
    });
    socket.on("game_start", function (result) {
      let message = result?.ok;
      let cardsOpposant = result?.cardsOpposant;

      console.log("game_start", message, cardsOpposant);

      if (message === "ok") {
        dispatch(setGameReady(true));
        dispatchCardsOpponent(cardsOpposant);
      } else {
        setMessage("Le combat n'a pas pu démarrer.");
        setSeverity("danger");
        setOpen(true);
      }
    });
    socket.on("game_turn", function (message) {
      console.log("game_turn", message);
      let yourTurn = message === user?.id;
      dispatch(setTurn(yourTurn));
    });
    socket.on("action_points", function (message) {
      console.log("action_points", message);
      dispatch(setPointAction(message));
    });

    socket.on("game-winner", function (winner) {
      console.log("game-winner", winner);
      dispatch(setGameWinned(winner));
    });
    socket.on("action_points_insuffisant", function (message) {
      console.log("action_points_insuffisant", message);
      setMessage("Vous n'avez pas assez de points d'action.");
      setSeverity("danger");
      setOpen(true);
    });
  }, [JSON.stringify(usersConnected)]);
  //Appelé au changement du user
  useEffect(() => {
    if (user != null) {
      var sessionId = uuidv4();
      socket.auth = { sessionId };
      socket.connect();
      send_data("user", "send-login", user.id);
    }
  }, [user]);
  //Appelé au unmount de l'application
  useEffect(() => {
    return function clean() {
      send_data("user", "disconnect", user?.id);
      window.location.reload(false);
    };
  }, []);
  //Appelé au changement des points de vie d'une carte
  useEffect(() => {
    socket.on("new-card-hp", function (message) {
      console.log("new-card-hp", message?.health, message?.id);
      findAndModifyCardAttaquee(message);
    });
  }, [JSON.stringify(cardsSelectionneesOpposant), JSON.stringify(cardsSelectionnees)]);

  /////////////////////////////////
  // Action dans le store        //
  /////////////////////////////////

  //Dispatch l'opposant séléctionné pour le combat
  const dispatchOpposant = (opposant) => {
    let tmpOpponent = usersConnected.find((x) => x.id === opposant);
    dispatch(setSelectedClientOpposant(tmpOpponent));
  };
  //Dispatch les cartes choisies par l'opposant
  const dispatchCardsOpponent = (cards) => {
    dispatch(setSelectedCardsOpponentJeu([...cards]));
  };
  //Trouve et dispatch les modifs d'une carte attaquée
  const findAndModifyCardAttaquee = (message) => {
    if (cardsSelectionneesOpposant?.find((x) => x.id === message?.id)) {
      let tmp = [...cardsSelectionneesOpposant];
      let index = cardsSelectionneesOpposant?.findIndex((x) => x.id === message?.id);
      tmp[index].hp = message?.health;
      dispatch(setSelectedCardsOpponentJeu([...tmp]));
    } else if (cardsSelectionnees?.find((x) => x.id === message?.id)) {
      let tmp = [...cardsSelectionnees];
      let index = cardsSelectionnees?.findIndex((x) => x.id === message?.id);
      tmp[index].hp = message?.health;
      dispatch(setSelectedCardsJeu([...tmp]));
    }
  };

  /////////////////////////////////
  // Général                     //
  /////////////////////////////////

  //A chaque utilisateur qui se connecte, on le récupère via un appel au back
  useEffect(() => {
    setUsersConnected([]);
    usersIdConnected.forEach((element) => {
      fetch(`${url}/user_id/${element}`)
        .then((resp) => resp.json())
        .then((result) => {
          setUsersConnected((usersConnected) => [...usersConnected, result]);
          console.log("userslist update");
        });
    });
  }, [JSON.stringify(usersIdConnected)]);

  //La liste des joueurs disponibles
  const listItems = usersConnected?.map((user) => (
    <tr className={[demandeCombatRecue || demandeCombatEnvoyee ? "" : "cursor-pointer", "d-flex"]} key={user.id}>
      <td style={{ flexBasis: "100%" }} className="d-flex justify-content-between align-items-center">
        <span>
          {user.userName} {user.lastName}
        </span>
        <FontAwesomeIcon icon="fas fa-gamepad" onClick={() => demanderCombat(user.id)} />
      </td>
      
    </tr>
  ));

  return (
    <div className="w-100 h-100" style={{ position: "relative" }}>
      <div className="d-flex">
        {/* Affiche les joueurs disponnibles */}
        <div style={{ flexBasis: "20%" }}>
          <table className="table table-striped table-bordered table-hover">
            <thead>
              <tr className="d-flex">
                <th style={{ flexBasis: "100%" }}>Name</th>
              </tr>
            </thead>
            <tbody className={demandeCombatRecue || demandeCombatEnvoyee ? "opacity-50" : ""}>{listItems}</tbody>
          </table>
          <Chat />
        </div>
        {/* Affiche l'interface du jeu */}
        <div className="p-5" style={{ flexBasis: "80%" }}>
          {/* L'interface de jeu du joueur connecté"*/}
          <div>
            <InterfaceJoueur user={user} selectionCard={selectionCard} opposant={false} />

            <i className="fas fa-5x fa-spinner fa-pulse"></i>
          </div>
          <hr />
          {/* Interface du joueur adverse */}
          <div>
            {/* Si l'opposant n'est pas choisi, on affiche une icone chargement */}
            {!opponent?.id && (
              <div>
                <FontAwesomeIcon icon="fas fa-spinner" />
              </div>
            )}
            {/* Si l'opposant a accepté notre demande, alors on affiche son interface de combat*/}
            {opponent?.id && selectionCard && (
              <div>
                <InterfaceJoueur user={opponent} selectionCard={false} opposant={true} />
              </div>
            )}
            {/* Si une demande de combat est réalisé et que nous recevons une demande de combat, on affiche la demande */}
            {demandeCombatRecue && !selectionCard && (
              <div>
                <DemandeReponseCombat accepterCombat={() => accepterCombat()} refuserCombat={() => refuserCombat()} />
              </div>
            )}
            {/* Si on demande un combat avec un adversaire on affiche notre demande */}
            {demandeCombatEnvoyee && !selectionCard && (
              <div>
                <DemandeCombat annulerCombat={() => annulerCombat()} />
              </div>
            )}
          </div>
        </div>
        {/* Code utilisé pour l'affichage de la snackBar pour notifier un joueur d'une action de la part de l'autre joueur */}
        <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
          <Alert severity={severity} onClose={handleClose} className={["w-100 "]}>
            {message}
          </Alert>
        </Snackbar>
      </div>
      {/* Si le combat est gagné */}
      {gameGagne &&(
            <CombatFin message={gameGagne === user?.id ? "Vous avez gagné !" : "Vous avez perdu !"} />
          
        )}
    </div>
  );
}

export default Play;
