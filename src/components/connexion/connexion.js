import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectedClient } from "../../core/client.selector";
import { setSelectedClient } from "../../core/client.actions.js";
import { Card } from "react-bootstrap";
import { url } from "../../conf";
import { Link } from "react-router-dom";

function ConnexionModule() {
  const [pwd, setPwd] = useState("");
  const [username, setUsername] = useState("");
  const user = useSelector(selectedClient);

  const dispatch = useDispatch();

  const selectClient = (id) => {
    dispatch(setSelectedClient(id));
  };

  const connect = () => {
    fetch(`${url}/auth`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: pwd,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result?.status === 403) alert("Wrong login/password");
        else {
          fetch(`${url}/user/${result?.username}`)
            .then((resp) => resp.json())
            .then((values) => {
              selectClient(values);
            });
        }
      });
  };

  const usernameChanged = (e) => {
    setUsername(e.target?.value);
  };

  const pwdChanged = (e) => {
    setPwd(e.target?.value);
  };

  return (
    <div style={{ height: "80vh" }}>
      <div className="d-flex flex-column align-items-center justify-content-center h-100 w-100">
        {user && user.id ? (
          <div>
            <div style={{ fontSize: "300%" }}>
              <b>You are already connected</b>
            </div>
            <div className="d-flex align-items-center justify-content-center">
              <Card style={{ backgroundColor: "rgb(219 218 218)" }}>
                <Card.Text className="p-3">
                  <Link to="/myCards" style={{ fontSize: "300%" }}>
                    My cards
                  </Link>
                </Card.Text>
              </Card>
              <Card style={{ backgroundColor: "rgb(219 218 218)" }} className="ms-2">
                <Card.Text className="p-3">
                  <Link to="/myCardsOnSale" style={{ fontSize: "300%" }}>
                    My cards on sale
                  </Link>
                </Card.Text>
              </Card>
              <Card style={{ backgroundColor: "rgb(219 218 218)" }} className="ms-2">
                <Card.Text className="p-3">
                  <Link to="/clientAddMoney" style={{ fontSize: "300%" }}>
                    Add money
                  </Link>
                </Card.Text>
              </Card>
            </div>
          </div>
        ) : (
          <div>
            <div>
              <div>
                <label>Username</label>
                <input className="form-control" type="text" name="username" value={username} onChange={usernameChanged} placeholder="dresseur1" />
              </div>
              <div className="mt-2 mb-2">
                <label>Password</label>
                <input className="form-control" type="password" name="pwd" value={pwd} onChange={pwdChanged} placeholder="test1234" />
              </div>
              <button className="btn btn-success w-100" onClick={connect}>
                Connect
              </button>
              <div>
                <h6>Dev only</h6>
                <hr/>
                <pre>
                  dresseur1:test1234
                  dresseur2:test1234
                  dresseur3:test1234
                </pre>
              </div>

            </div>

            <div className="mb-2 text-center">
              <Link to="/newUser">New user</Link>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default ConnexionModule;
