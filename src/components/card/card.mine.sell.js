import React, { useEffect, useState } from "react";
import CardGeneral from "./card.general.js";
import CardList from "./card.list.js";
import { url } from "../../conf";
import { selectedCardId, selectedClient } from "../../core/client.selector";
import { useSelector } from "react-redux";

function CardMineSell() {
  const [cardSelected, selectCard] = useState(null);
  const cardSelectedId = useSelector(selectedCardId);
  const [cards, setCards] = useState();
  const user = useSelector(selectedClient);


  useEffect(() => {
    if (cardSelectedId) {
      fetch(`${url}/card/${cardSelectedId}`)
        .then((resp) => resp.json())
        .then((values) => {
          selectCard(values);
        });
    }
  }, [cardSelectedId]);

  useEffect(() => {
    fetch(`${url}/cards_to_sell/${user?.id}`)
      .then((resp) => resp.json())
      .then((values) => {
        setCards([]);
        setCards(values);
      }).catch(error=>{});
  }, [user]);


  return (
    <div className="w-100 d-flex p-3">
      <div style={{ flexBasis: "50%" }} className="flex-grow-1">
        <h3>My cards on sale</h3>
        <CardList cards={cards}/>
      </div>
      {cardSelected && (
        <div className="d-flex justify-content-center align-items-center w-100" style={{ flexBasis: "50%" }}>
          <div  className="mt-5">
            <CardGeneral card={cardSelected} />
          </div>
        </div>
      )}
    </div>
  );
}

export default CardMineSell;
