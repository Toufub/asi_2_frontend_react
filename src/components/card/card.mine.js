import React, { useEffect, useState } from "react";
import CardGeneral from "./card.general.js";
import CardList from "./card.list.js";
import { selectedCardId } from "../../core/client.selector";
import { selectedClient } from "../../core/client.selector";
import { url } from "../../conf";
import { useSelector } from "react-redux";

function CardMine() {
  const [cardSelected, selectCard] = useState(null);
  const cardSelectedId = useSelector(selectedCardId);
  const user = useSelector(selectedClient);
  const [cards, setCards] = useState();

  useEffect(() => {
    if (cardSelectedId) {
      fetch(`${url}/card/${cardSelectedId}`)
        .then((resp) => resp.json())
        .then((values) => {
          selectCard(values);
        });
    }
  }, [cardSelectedId]);

  useEffect(() => {
    fetch(`${url}/cards`)
      .then((resp) => resp.json())
      .then((values) => {
        setCards([]);
        setCards(values.filter(x => x.userId === user?.id));
      });
  }, [user]);

 return (
    <div className="w-100 d-flex p-3">
      <div style={{ flexBasis: "50%" }} className="flex-grow-1">
        <h3>My cards</h3>
        <CardList cards={cards}/>
      </div>
      {cardSelected && (
        <div className="d-flex justify-content-center align-items-center w-100" style={{ flexBasis: "50%" }}>
          <div  className="mt-5">
            <CardGeneral card={cardSelected} />
          </div>
        </div>
      )}
    </div>
  );
}

export default CardMine;
