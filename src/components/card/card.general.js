import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "react-bootstrap";

function CardGeneral(props) {

  return (
    <Card style={{minWidth:"200px",minHeight:"300px", border:props.card.hp <= 0 ? "2px solid var(--bs-danger)" : props.selectionne  ? "2px solid var(--bs-info)" : ""}}>
      <Card.Title>
        <div className="d-flex align-items-center justify-content-between">
          <div>
            <FontAwesomeIcon icon="bolt" />
            {props.card.energy}
          </div>
          {props.card.family}
          <div>
            {props.card.hp}
            <FontAwesomeIcon icon="heart" />
          </div>
        </div>
      </Card.Title>
      <Card.Body>
        <div className="d-flex align-items-center justify-content-center">{props.card.name}
        <div className="ms-3">
            {props.card.attack}
            <FontAwesomeIcon icon="khanda" />
          </div></div>
        {props.card.hp > 0 && <img src={props.card.imgUrl} alt="imgCard" style={{width:"30vh"}} />}
        {props.card.hp <= 0 && <FontAwesomeIcon icon="fa-solid fa-skull-crossbones fa-2x" />}
        <div> {props.card.description}</div>
      </Card.Body>
    </Card>
  );
}

export default CardGeneral;
