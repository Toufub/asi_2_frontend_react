import React, { useEffect, useState } from "react";
import CardGeneral from "./card.general.js";
import CardList from "./card.list.js";
import { selectedCardId } from "../../core/client.selector";
import { url } from "../../conf";
import { selectedClient } from "../../core/client.selector";
import { useDispatch, useSelector } from "react-redux";
import { removeSelectedCardId, setSelectedClient } from "../../core/client.actions.js";

function CardSell() {
  const [price, setPrice] = useState(0);
  const [cardSelected, selectCard] = useState(null);
  const cardSelectedId = useSelector(selectedCardId);
  const [cardsInSales, setCardsInSales] = useState();
  const [cardsNotToSell, setCardsNotToSell] = useState();
  const user = useSelector(selectedClient);

  const dispatch = useDispatch();

  const priceChanged = (e) => {
    setPrice(e.target?.value);
  };

  useEffect(() => {
    if (cardSelectedId) {
      fetch(`${url}/card/${cardSelectedId}`)
        .then((resp) => resp.json())
        .then((values) => {
          selectCard(values);
        });
    }
  }, [cardSelectedId]);

  useEffect(() => {
    getCardsInSale();
    getCardsNotForSale()
  }, [user]);

  function getCardsInSale() {
    fetch(`${url}/cards_to_sell`)
        .then((resp) => resp.json())
        .then((values) => {
          setCardsInSales([]);
          setCardsInSales(values.filter(x=> x.price > 0));
        });
  }

  function getCardsNotForSale(){
    fetch(`${url}/cards`)
        .then((resp) => resp.json())
        .then((values) => {
          setCardsNotToSell([]);
          setCardsNotToSell(values.filter(x => (x.price === 0 || x.price === null) && x.userId === user?.id));
      });
  }


  const sell = () => {
    fetch(`${url}/transaction/sell`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        card_id: cardSelectedId,
        user_id: user.id,
        price: price
      }),
    })
    .then((resp) => resp.json())
    .then((value) => {
      console.log(user, value)
      fetch(`${url}/user/${user.login}`)
        .then((resp) => resp.json())
        .then((values) => {
          dispatch(setSelectedClient(values));
          dispatch(removeSelectedCardId(cardSelectedId));
          getCardsNotForSale();
          getCardsInSale();
        });
    });
  };

  return (
    <div className="w-100 d-flex p-3">
      <div style={{ flexBasis: "50%" }} className="flex-grow-1">
        <h3>Not on sale</h3>
        <CardList cards={cardsNotToSell} clickCard={true} />
        <div style={{ marginBottom: "50px" }}></div>
        <div className="mt-3">
          <h3>Your card in sale</h3>
          <CardList cards={cardsInSales} clickCard={true} />
        </div>
      </div>
      {cardSelected && (
        <div className="d-flex justify-content-center align-items-center w-100" style={{ flexBasis: "50%" }}>
          <div className="mt-5">
            <CardGeneral card={cardSelected} />
            <div className="text-center mt-2">
              {
                <div>
                  <div className="mt-2 mb-2">
                    <label>Price</label>
                    <input className="form-control" min="0" type="number" name="price" value={price} onChange={priceChanged} placeholder="100" />
                  </div>
                  <button className="btn btn-success" onClick={sell}>
                    Sell {price}€
                  </button>
                </div>
              }
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default CardSell;
