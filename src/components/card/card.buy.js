import React, { useEffect, useState } from "react";
import CardGeneral from "./card.general.js";
import { url } from "../../conf";
import CardList from "./card.list.js";
import { selectedCardId } from "../../core/client.selector";
import { selectedClient } from "../../core/client.selector";
import { useDispatch, useSelector } from "react-redux";
import { setSelectedClient } from "../../core/client.actions.js";

function CardBuy() {
  const [cardSelected, selectCard] = useState(null);
  const cardSelectedId = useSelector(selectedCardId);
  const [cards, setCards] = useState();
  const user = useSelector(selectedClient);

  const dispatch = useDispatch();

  useEffect(() => {
    if (cardSelectedId) {
      fetch(`${url}/card/${cardSelectedId}`)
        .then((resp) => resp.json())
        .then((values) => {
          selectCard(values);
        });
    }
  }, [cardSelectedId]);

  useEffect(() => {
    getCardsOnSales();
  }, [user]);

  function getCardsOnSales() {
    fetch(`${url}/cards_to_sell`)
        .then((resp) => resp.json())
        .then((values) => {
          setCards([]);
          setCards(values.filter(x => x.userId !== user?.id));
        });
  }

  function updateUserBalance() {
    fetch(`${url}/user/${user.login}`)
        .then((resp) => resp.json())
        .then((value)=>{
          dispatch(setSelectedClient(value));
        }).catch(error=>{
          alert("Error  : "+ error)
    })
  }


  const buy = () => {
    fetch(`${url}/transaction/buy`, {
      method: "POST",
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        card_id: cardSelectedId,
        user_id: user.id,
      }),
    }).then((resp) => resp.json())
      .then(() => {
        getCardsOnSales();
        updateUserBalance();
        selectCard(null);
      }).catch(error=>{
      alert("Erreur : " + error)
    })
  };

  return (
    <div className="w-100 d-flex p-3">
      <div style={{ flexBasis: "50%" }} className="flex-grow-1">
        <h3>Market</h3>
        <CardList cards={cards}/>
      </div>
      {cardSelected && (
        <div className="d-flex justify-content-center align-items-center w-100" style={{ flexBasis: "50%" }}>
          <div  className="mt-5">
            <CardGeneral card={cardSelected} />
            <div className="text-center mt-2">
              <button className="btn btn-success" onClick={buy}>
                Buy {cardSelected?.price}€
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default CardBuy;
