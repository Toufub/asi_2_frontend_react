import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Menu() {

  return (
    <div>
      <div style={{ fontSize: "400%" }} className="p-5 h-100 w-100">
        <div className="d-flex align-items-center justify-content-around">
          <Card style={{backgroundColor: "rgb(219 218 218)"}}>
            <Card.Text className="p-3" style={{margin: 40}}>
              <FontAwesomeIcon icon="fa-cart-shopping" />
              <Link style={{color: "rgb(0 0 0)" }} className="p-3" to="/buy">Buy</Link>
            </Card.Text>
          </Card>
          <Card style={{backgroundColor: "rgb(219 218 218)"}}>
            <Card.Text className="p-3" style={{margin: 40}}>
              <FontAwesomeIcon icon="euro-sign" />
              <FontAwesomeIcon icon="euro-sign" />
              <Link style={{color: "rgb(0 0 0)" }} className="p-3" to="/sell">Sell</Link>
            </Card.Text>
          </Card>
        </div>
        <div className="d-flex align-items-center justify-content-around">
          <Card style={{backgroundColor: "rgb(219 218 218)" }}>
            <Card.Text className="p-3" style={{margin: 40}}>
              <FontAwesomeIcon icon="fa-gamepad" />
              <Link style={{color: "rgb(0 0 0)" }} className="p-3" to="/play">Play</Link>
            </Card.Text>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default Menu;
