import { useState } from "react";
import { useSelector } from "react-redux";
import { selectedClient } from "../../core/client.selector";
import { url } from "../../conf";

function ClientAddMoney() {

  const [amount, setAmount] = useState("");
  const user = useSelector(selectedClient);


  const amountChanged = (e) => {
     setAmount(e.target?.value);
   };

   const validate = () => {
     fetch(`${url}/user/${user.login}`, {
       method: "PUT",
       headers: {
         "Content-Type": "application/json",
       },
       body: JSON.stringify({
         balance: amount,
       }),
     })
       .then((response) => response.json())
       .then(() => {
         
       });
   };

  return (
    <div className="d-flex flex-column align-items-center mt-5">
      <div className="mt-2 mb-2">
        <label>Amount</label>

        <input className="form-control d-flex" type="number" name="amount" value={amount} onChange={amountChanged} placeholder="50000"/>
      </div>
      <button className="btn btn-success" onClick={validate}>
        Validate
      </button>
    </div>
  );
}

export default ClientAddMoney;
