FROM nginx:stable-alpine

COPY ./build/ /usr/share/nginx/html/
COPY ./webserver.conf /etc/nginx/conf.d/default.conf

